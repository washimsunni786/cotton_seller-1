import React from 'react'
import { Provider } from 'react-native-paper'
import { NavigationContainer } from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack';
import { theme } from './src/core/theme'
import {
  LoginScreen,
  RegisterScreen,
  ForgotPasswordScreen,
  SplashScreen,
  Dashboard,
  SetPasswordScreen,
  VerifyOtpScreen,
  ChangePasswordScreen,
  SearchSelectSeller,
  DealDetails,
  NegotiateDetails,
  NotificationSelectSeller,
  MyPostDetails,
  MyContractFilter,
  MyContractDetails,
} from './src/screens'

const Stack = createStackNavigator()

const App = () => {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="SplashScreen"
          screenOptions={{
            headerShown: false,
          }}
        >
          
          <Stack.Screen name="LoginScreen" component={LoginScreen} />
          <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
          <Stack.Screen
            name="ForgotPasswordScreen"
            component={ForgotPasswordScreen}/>
            <Stack.Screen
            name="SetPasswordScreen"
            component={SetPasswordScreen}/>
            <Stack.Screen
            name="ChangePasswordScreen"
            component={ChangePasswordScreen}/>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="Dashboard" component={Dashboard} />
          <Stack.Screen name="VerifyOtpScreen" component={VerifyOtpScreen} />
		  <Stack.Screen name="MyContractDetails" component={MyContractDetails} />
		  <Stack.Screen name="MyContractFilter" component={MyContractFilter} />
          <Stack.Screen name="SearchSelectSeller" component={SearchSelectSeller} />
          <Stack.Screen name="DealDetails" component={DealDetails} />
          <Stack.Screen name="NegotiateDetails" component={NegotiateDetails} />
          <Stack.Screen name="NotificationSelectSeller" component={NotificationSelectSeller} />
          <Stack.Screen name="MyPostDetails" component={MyPostDetails} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}

export default App
